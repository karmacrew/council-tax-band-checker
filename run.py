import requests
import os
import re
from bs4 import BeautifulSoup
LIVE = True

def getNumPages(s):
    try:
        s = str(s).strip()
        m = re.search(r'Page 1&nbsp;\n.*of&nbsp;\n.*([0-9])', s)
        numPages = int(m.group(1))
        return numPages
    except:
        return 1

def getDoeCode(s):
    try:
        soup = BeautifulSoup(s, 'lxml')
        doeCode = soup.find('input', {'id':'txtDoeCode'}).get('value')
        return str(doeCode)
    except:
        print "Not found"
        pass

def isMultiPage(s):
    try:
        return getNumPages(s) > 1
    except:
        pass
    else:
        return False

def buildRequest(showAll = False, **kwargs):
    url = 'http://cti.voa.gov.uk/cti/InitS.asp?lcn=0'
    headers = {
        'Pragma': 'no-cache',
        'Origin': 'http://cti.voa.gov.uk',
        'Accept-Encoding':'gzip,deflate',
        'Accept-Language':'en-GB,en-US',
        'Upgrade-Insecure-Requests':'1',
        'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
        'Content-Type':'application/x-www-form-urlencoded',
        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Cache-Control':'no-cache',
        'Referer':'http://cti.voa.gov.uk/cti/inits.asp',
        'Connection':'keep-alive'
    }
    payload = {
        'btnPush':'1',
        'txtDoeCode':'',
        'txtNameNum':'',
        'txtStreet':'',
        'txtPostalDistrict':'',
        'txtPDSpecific':'',
        'txtTown':'',
        'txtBillRef':'',
        'txtStartKey':'0',
        'txtPageNum':'0',
        'txtBack':'',
        'txtBAName':'',
        'txtBAWeb':'',
        'lstBand':'',
        'lstBandStatus':'',
        'lstCourtCode':'',
        'lstPartDomestic':'',
        'txtPageSize':'',
        'txtUpdateDate':'',
        'txtPF':'',
        'txtPickedSubSt':'',
        'txtPickedStreet':'',
        'txtPickedTown':'',
        'txtLastStreetResp':'',
        'txtLastPDResp':'',
        'txtLastTownResp':'',
        'txtStreetSelected':'',
        'intNumFound':'',
        'intNumStreets':'',
        'blnPaging':'',
        'lstBA':'',
        'txtRedirectTo':'InitS.asp',
        'txtPostCode': line
    }
    if showAll:
        payload['lstPageSize'] = 50
        payload['txtDoeCode'] = getDoeCode(kwargs['previousReq'])
        payload['txtPageNum'] = 1
        payload['txtPageSize'] = 50
        payload['lstBA'] = payload['txtDoeCode']
     

    return {'url':url, 'headers':headers, 'payload':payload}

def isValidResult(s):
    if "The following properties within" in s:
        return True
    else:
        return False

def parseResult():
    return None


POSTCODES_FILE= open('list.txt')

line = POSTCODES_FILE.readline()

while line:
    print line
    #Remove newline
    line = line.rstrip()

    def store_result():
        #Check if already processed
        if os.path.isfile("results/" + line + ".html"):
            print "Skipping " + line

        if LIVE:
            request = buildRequest()
            r = requests.post(data=request['payload'], headers=request['headers'], url=request['url']).text
        else:
            r = f.read()
        #Only store responses with a valid result
        if isValidResult(r):
            if isMultiPage(r):
                print "Performing show all request"
                request = buildRequest(showAll=True, previousReq=r)
                r = requests.post(data=request['payload'], headers=request['headers'], url=request['url']).text
            else:
                print "No need to do show all request"

            #Write out result to file
            f = open("results/" + line.encode('utf-8') + ".html", 'w+')
            f.write(r.encode('utf-8'))
            f.close()
        else:
            #Store fail in fail.log
            failog = open("results/fail.log", "a+")
            #failog.write(line.encode('ascii') + '\n')
            failog.close()
    store_result()
    #Read next line
    line = POSTCODES_FILE.readline()

POSTCODES_FILE.close()

