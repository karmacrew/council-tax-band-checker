# Council Tax Band Checker

Compare regionally the council tax bands of all (known) addresses in the UK by address and postcode.
Data from ONS.

## Scraper: Combine postcode data with tax band

First we need to collect the data, then add the tax band to each address found via VOA.

### Usage

- Create (or get) list of postcodes into `list.txt`

- Run collector

    python run.py


## Contribute 

    git clone git@gitlab.com:karmacrew/council-tax-band-checker.git
