import os
from bs4 import BeautifulSoup
import json

def parse(s, fp):
    soup = BeautifulSoup(s, 'lxml')
    # Loop each row in results table
    rows = soup.find_all('tr')
    houses = []
    for row in rows:
        # Get each UARN, postc, housenumber, Tax band, Improvement Indicator ref number
        uarn = getUARN(row)
        buildingNumber = getBuildingNumber(row)
        taxBand = getTaxBand(row)
        improvementIndicator = getImprovementIndicator(row)
        localAuthorityReferenceNum = getLocalAuthorityReferenceNum(row)
        postCode = fp.name.replace('./results/', '').replace('.html','')

        house = {'uarn':uarn,'buildingNumber':buildingNumber,
                 'taxBand':taxBand, 'improvementIndicator':improvementIndicator,
                 'localAuthorityReferenceNum':localAuthorityReferenceNum,
                 'postCode':postCode}
        houses.append(house)
        
    # Return list of each property as a dictionary
    return houses

def getUARN(row):
    try:
        uarn = row.contents[1].contents[1].contents[0]['href']
        uarn = uarn.replace('JavaScript:SP(', '')
        uarn = uarn.replace(')', '')
        print "The uarn is: " + uarn
        return uarn
    except:
        return ''

def getBuildingNumber(row):
    try:
        buildingNumber = row.contents[1].contents[1].contents[0].string
        buildingNumber = str(buildingNumber.strip().split(',')[0])
        print "The building number is " + buildingNumber
        return buildingNumber
    except:
        return ''

def getTaxBand(row):
    try:
        taxBand = str(row.contents[3].contents[0].strip())
        print "The tax band is: " + taxBand
        return taxBand
    except:
        return ''

def getImprovementIndicator(row):
    try:
        if 'indicator' in str(row.contents[5]):
            improvementIndicator = 'Y'
        else:
            improvementIndicator = 'N'
        print "Improvement indicator is: " + improvementIndicator
        return improvementIndicator
    except:
        return ''

def getLocalAuthorityReferenceNum(row):
    try:
        localAuthorityReferenceNum = row.contents[7].contents[0].string.strip()
        print "Local authority ref number is: " + localAuthorityReferenceNum
        return localAuthorityReferenceNum
    except:
        return ''


def store(houses):
    fpResult = open('./parsed/result.json','a+')
    #import pdb; pdb.set_trace()
    for house in houses:
        fpResult.write(json.dumps(house) + ",")
    fpResult.close()
    print houses


#Loop each result and parse it
for f in os.listdir('./results'):
    fp = open('./results/' + f)
    result = fp.read()
    fp.close()
    houses = parse(result, fp)
    store(houses)

